---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: page
---

# Eurocrypt 2020

## Overview

- [Official conference website](https://eurocrypt.iacr.org/2020/)
- Eurocrypt 2020 will take place in Zagreb, Croatia on May 10-14, 2020
- There are [affiliated events](https://eurocrypt.iacr.org/2020/affiliatedevents.html) on Saturday, May 9th and Sunday, May 10th

## Train Journeys

There are direct trains to Zagreb from Munich and Budapest, which means you can reach
Zagreb with only one change from a lot of cities in western Europe.

TODO: indicate for each one which countries will be traversed

### Germany

- [Munich](https://www.seat61.com/international-trains/trains-from-Munich.htm#Munich-Croatia):
  direct day train, or direct sleeper train
- [Berlin](https://www.seat61.com/international-trains/trains-from-Berlin.htm#Berlin-Zagreb):
  one change either in Munich or in Budapest. Daytime and sleeper train options available for both.
- Karlsruhe: via Munich
- Darmstadt:
- Bochum:
- Lübeck:
- Hamburg:
- Saarbrücken:
- Wuppertal:
- Erlangen:
- Nürnberg:

### France

- [Paris](https://www.seat61.com/international-trains/trains-from-Paris.htm#Paris-Croatia):
  one change in Munich. Either stay overnight in Munich, or take a night train from Munich to Zagreb.
- Rennes:
- Bordeaux:
- Nancy:
- Metz:
- Lyon:
- Rouen:
- Limoges:

### England

- [London](https://www.seat61.com/Croatia.htm#London%20to%20Ljubljana%20&%20Zagreb):
  change in Paris and Munich. Direct sleeper train from Munich, or stay overnight in
  Munich and take a direct daytime train to Zagreb.
- Guildford:
- Birmingham:
- Bristol:
- Oxford:
- Cambridge:
- York:

### Scotland

- Edinburgh:

### Switzerland

- [Zurich](https://www.seat61.com/international-trains/trains-from-Switzerland.htm#Switzerland-Croatia):
  direct sleeper train to Zagreb, or during daytime with one change in Salzburg.
- Lausanne:

### Spain

- Madrid:
- Barcelona:

### Denmark

- Kopenhagen:
- Aarhus:

### Netherlands

- Eindhoven:
- Radboud:
- Amsterdam:
- Delft:

### Belgium

- Brussels:
- Diegem:
- Leuven:

### Israel

- Tel Aviv:
- Herzliya:
- Rehovot:
- Ramat Gan:
- Haifa:

### Austria:

- Graz:
- Vienna:
- Klagenfurt:

### Czech Republic

- Prague:

### Norway

- Bergen:

### Sweden

- Lund:

### Italy

- Rome:

### Russia

- Kaliningrad:

### Estonia

- Tartu:
