---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: page
---

FAQ
---



I live overseas, cannot come by train!?
===
Consider if participating remotely is an option. TODO Link to this
  Carbon-neutral conference website I once found. 
  If you fly, consider paying a carbon offset.

Why?
===
Carbon emissions from fossil fuel have led and lead to a climate crisis
Flying emits a factor 5 to 50 more than train

How to use the time in the train?
===
Get privacy screens for your mobile devices and work. Trains usually
  offer more space than planes. Noise cancelling headphones or earplugs
  might be useful, too. (include carbon emissions of those gadgets)

It's the first time I do such a long journey, what do I need to consider?
===
- transit visas
- it's a long journey, make sure you feel comfortable and safe
- mobile phone contract/roaming

How to book?
===
no traveller guarantees if split booking

What about carbon offsets?
===
- avoiding is more important than offsetting
- if you want to offset, consider using an independent provider, not the
  airline directly. For example Atmosfair, they have high standards.

Some trains run on fossil fuel, what's the factor here?
===
- TODO calculate this, for example for some journey in the UK.
- TODO compare what electricity different train companies use

Changing habits as a community
===
- post your pictures with #EurocryptByTrain on Twitter, Instagram,
  Mastodon, Pixelfed.

